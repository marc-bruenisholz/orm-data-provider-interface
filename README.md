# orm-data-provider-interface

Contains only the interface that is used by the package property-path-tree-builder to retrieve 
orm data from an external component that implements this interface.

This repository is part of the project SQLQueriesByPropertyPaths.