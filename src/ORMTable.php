<?php

namespace ch\_4thewin\ORMDataProviderInterface;

class ORMTable
{
    /**
     * The name of the table in the database.
     * @var string
     */
    protected string $tableName;

    /**
     * @var ORMColumn[]
     */
    protected array $primaryKeyColumns;

    /**
     * @param string $tableName
     * @param ORMColumn[] $primaryKeyColumns
     */
    public function __construct(string $tableName, array $primaryKeyColumns)
    {
        $this->tableName = $tableName;
        $this->primaryKeyColumns = $primaryKeyColumns;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @return ORMColumn[]
     */
    public function getPrimaryKeyColumns(): array
    {
        return $this->primaryKeyColumns;
    }
    
}