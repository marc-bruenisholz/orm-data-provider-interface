<?php

namespace ch\_4thewin\ORMDataProviderInterface;


abstract class ORMSingleCardinalityAssociation extends ORMAssociation
{
    /**
     * This is the table holding the foreign key column.
     * @var ORMTable
     */
    protected ORMTable $owningTable;

    /**
     * @var ORMColumn Contains the foreign key column name. The foreign keys refer to primary keys of other tables.
     */
    protected ORMColumn $foreignKeyColumn;

    /**
     * The table of which the primary
     * keys are used in the
     * foreign key column of the
     * table owning the relationship.
     * @var ORMTable
     */
    protected ORMTable $inverseTable;

    /**
     * @param ORMEntity $sourceOrmEntity
     * @param string $propertyName
     * @param ORMEntity $targetOrmEntity
     * @param ORMTable $owningTable
     * @param ORMColumn $foreignKeyColumn
     * @param ORMTable $inverseTable
     */
    public function __construct(ORMEntity $sourceOrmEntity,
                                string    $propertyName,
                                ORMEntity $targetOrmEntity,
                                ORMTable  $owningTable,
                                ORMColumn $foreignKeyColumn,
                                ORMTable  $inverseTable)
    {
        parent::__construct($sourceOrmEntity, $propertyName, $targetOrmEntity);
        $this->owningTable = $owningTable;
        $this->foreignKeyColumn = $foreignKeyColumn;
        $this->inverseTable = $inverseTable;
    }

    /**
     * @return ORMTable
     */
    public function getOwningTable(): ORMTable
    {
        return $this->owningTable;
    }

    /**
     * @return ORMTable
     */
    public function getInverseTable(): ORMTable
    {
        return $this->inverseTable;
    }

    /**
     * @return ORMColumn
     */
    public function getForeignKeyColumn(): ORMColumn
    {
        return $this->foreignKeyColumn;
    }

}