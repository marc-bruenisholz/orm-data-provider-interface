<?php
/*
 * Copyright 2022 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace ch\_4thewin\ORMDataProviderInterface;

class ORMColumn
{
    /**
     * @var string
     */
    protected string $columnName;

    /**
     * @var string One of integer, boolean, float, string
     */
    protected string $columnType;

    protected bool $isNullable;

    /**
     * The provided string should use the table reference variable tableReference to
     * access different columns on the table and/or the column reference variable columnReference
     * to apply functions to change the resulting value. Use the syntax in the examples.
     * Example 1: Convert column value to upper case
     * "upper(${columnReference})"
     * Example 2: Calculate time difference in seconds between two timestamps
     * "(strftime('%s', ${tableReference}.ended_at) - strftime('%s', ${tableReference}.started_at))"
     *
     * If no template is given, the default template is used which resolves to an expression like
     * "`tableNameOrAlias`.`columName`".
     * @var string|null A template string that resolves to a valid column expression.
     */
    protected ?string $columnExpressionTemplate;

    /**
     * @param string $columnName
     * @param string $columnType
     * @param string|null $columnExpressionTemplate
     */
    public function __construct(string $columnName, string $columnType, bool $isNullable, ?string $columnExpressionTemplate = null)
    {
        $this->columnName = $columnName;
        $this->columnType = $columnType;
        $this->isNullable = $isNullable;
        $this->columnExpressionTemplate = $columnExpressionTemplate;
    }

    /**
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName;
    }

    /**
     * @return string
     */
    public function getColumnType(): string
    {
        return $this->columnType;
    }

    /**
     * @return string|null
     */
    public function getColumnExpressionTemplate(): ?string
    {
        return $this->columnExpressionTemplate;
    }

    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->isNullable;
    }



}