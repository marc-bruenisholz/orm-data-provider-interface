<?php

namespace ch\_4thewin\ORMDataProviderInterface;

class ORMOneToOne extends ORMSingleCardinalityAssociation
{
    /** @var bool */
    protected bool $isFromInverseToOwningTable;

    /**
     * SingleCardinalityRelationship constructor.
     * @param ORMEntity $ormSourceEntity
     * @param string $propertyName
     * @param ORMEntity $ormTargetEntity
     * @param ORMTable $owningTable
     * @param ORMColumn $foreignKeyColumn
     * @param ORMTable $inverseTable
     * @param bool $isFromInverseToOwningTable
     */
    public function __construct(
        ORMEntity $ormSourceEntity,
        string    $propertyName,
        ORMEntity $ormTargetEntity,
        ORMTable  $owningTable,
        ORMColumn $foreignKeyColumn,
        ORMTable  $inverseTable,
        bool      $isFromInverseToOwningTable
    )
    {
        parent::__construct($ormSourceEntity, $propertyName, $ormTargetEntity, $owningTable, $foreignKeyColumn, $inverseTable);
        $this->isFromInverseToOwningTable = $isFromInverseToOwningTable;
    }

    /**
     * @return bool
     */
    public function isFromInverseToOwningTable(): bool
    {
        return $this->isFromInverseToOwningTable;
    }
}