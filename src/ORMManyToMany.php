<?php
/*
 * Copyright 2022 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace ch\_4thewin\ORMDataProviderInterface;

class ORMManyToMany extends ORMAssociation
{
    protected ORMTable $firstTable;

    protected ORMTable $intermediaryTable;

    protected ORMTable $secondTable;

    /**
     * @param ORMEntity $sourceOrmEntity
     * @param string $propertyName
     * @param ORMEntity $targetOrmEntity
     * @param ORMTable $firstTable
     * @param ORMTable $intermediaryTable
     * @param ORMTable $secondTable
     */
    public function __construct(ORMEntity  $sourceOrmEntity,
                                string    $propertyName,
                                ORMEntity $targetOrmEntity,
                                ORMTable  $firstTable,
                                ORMTable  $intermediaryTable,
                                ORMTable  $secondTable)
    {
        parent::__construct($sourceOrmEntity, $propertyName, $targetOrmEntity);
        $this->firstTable = $firstTable;
        $this->intermediaryTable = $intermediaryTable;
        $this->secondTable = $secondTable;
    }

    /**
     * @return ORMTable
     */
    public function getFirstTable(): ORMTable
    {
        return $this->firstTable;
    }

    /**
     * @return ORMColumn
     */
    public function getFirstForeignKeyColumn(): ORMColumn
    {
        return $this->intermediaryTable->getPrimaryKeyColumns()[0];
    }

    /**
     * @return ORMTable
     */
    public function getIntermediaryTable(): ORMTable
    {
        return $this->intermediaryTable;
    }

    /**
     * @return ORMColumn
     */
    public function getSecondForeignKeyColumn(): ORMColumn
    {
        return $this->intermediaryTable->getPrimaryKeyColumns()[1];
    }

    /**
     * @return ORMTable
     */
    public function getSecondTable(): ORMTable
    {
        return $this->secondTable;
    }
}