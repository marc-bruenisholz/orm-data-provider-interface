<?php

namespace ch\_4thewin\ORMDataProviderInterface;

interface ORMParent
{
    function getTargetOrmEntity(): ORMEntity;
}