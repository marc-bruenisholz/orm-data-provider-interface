<?php

namespace ch\_4thewin\ORMDataProviderInterface;

abstract class ORMProperty
{
    protected ORMEntity $sourceOrmEntity;

    protected string $propertyName;

    /**
     * @param ORMEntity $sourceOrmEntity
     * @param string $propertyName
     */
    public function __construct(ORMEntity $sourceOrmEntity, string $propertyName)
    {
        $this->sourceOrmEntity = $sourceOrmEntity;
        $this->propertyName = $propertyName;
    }

    /**
     * @return ORMEntity
     */
    public function getSourceOrmEntity(): ORMEntity
    {
        return $this->sourceOrmEntity;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

}